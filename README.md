## List of fighters ##
The second task fom [Binary studio](https://binary-studio.com/)

### Goals ###
- ✅ Create a repository on Bitbucket 
- ✅ Use new features of ES6 javascript for this project
- ✅ Use webpack or gulp for this project
- ✅ Create fighters list using OOP approach 

### Instruction ###
- Clone this repository
- ``` npm install ``` (make sure all dependencies are installed)
- ``` npm run dev ``` it will create a  **dist** folder with **bundle.js** file and a local sever will be run
- visit _localhost:8080_
