import FightersContainer from './components/FightersContainer';
import FightersService from './components/FightersService';
import Arena from  './components/Arena';
import View from './libs/View';

export default class App extends View {
    constructor() {
        super();

        this.fightersService = null;
        this.fighters = null;
        this.arena = null;
    }

    async init() {
        this.fightersService = new FightersService();
        this.fighters = new FightersContainer();
        this.fighters.on('requireFighterData', this.onDataRequired, this);
        this.fighters.on('fighterWasChosen', this.onFighterWasChosen, this);

        this.arena = new Arena();
        this.arena.on('arenaIsActivated', this.onArenaIsActivated, this);
        this.arena.on('showMessage', this.onShowMessage, this);

        App.rootElement.append(this.fighters.conteiner, this.arena.view);
    }

    async start() {
        try {
            const fighters = await this.fightersService.getAllFighters();
            this.fighters.createAllFighters(fighters);
        } catch (error) {
            console.warn(error);
            const warning = this.createElement({tagName: 'span', className: 'warning'});
            warning.append(document.createTextNode('Loading error...'));

            App.loadingElement.classList.toggle("loading-overlay-hidden");
            App.loadingElement.append(warning);
        } finally {
            App.loadingElement.classList.toggle("loading-overlay-hidden");
        }
    }

    async onDataRequired({id}) {
        try {
            const data = await this.fightersService.getDetails(id);
            this.fighters.setFightersData(id, {...data});
        } catch (err) {
            console.warn(err);
        }
    }

    onFighterWasChosen(fighter){
        this.arena.addFighter(fighter);
    }

    onArenaIsActivated(){
        this.fighters.hideAllDescriptions();
        this.fighters.hide()
    }

    onShowMessage(name){
        const sign = this.createElement({tagName: 'div', className: 'alert'});
        sign.appendChild(this.createElement({tagName: 'p'})).append(`${name} HAS WON !!!!`);
        App.rootElement.append(sign);
    }

    static rootElement = document.getElementById('app');
    static loadingElement = document.getElementById('loading-overlay');
}