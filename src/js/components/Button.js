import View from "../libs/View";

export default class Button extends View {
    constructor(fighterId, config) {
        super();

        this._fighterId = fighterId;
        this._isHidden = true;
        this._view = null;

        this.create(config);
    }

    create(config) {
        this.view = this.createElement(config);
        this.view.addEventListener('click', (event) => {
            this.emit('fighterChosen', {event, id: this._fighterId});
        });
    }

    show() {
        this._isHidden = false;
        this.view.id = '';
    }

    hide() {
        if(this._isHidden) return;
        this._isHidden = true;
        this.view.id = 'fighter-button-hidden';
    }


    get view() {
        return this._view;
    }

    set view(view) {
        this._view = view;
    }
}