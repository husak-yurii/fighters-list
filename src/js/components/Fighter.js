import View from "../libs/View";

/**
 * @class Fighter
 * @extends View
 *
 * */

export default class Fighter extends View {
    /**
     * @param {Object} fighter - properties for creating a view of a fighter
     * */
    constructor(fighter) {
        super();

        this._id = null;

        this._health = null;
        this._attack = null;
        this._defense = null;

        this._view = null;
        this._hasDescription = false;
        this._wasChosen = false;

        this.create(fighter);
        this.view.addEventListener('click', (event) => this.emit('fighterClicked', {event, id: this.id}));
    }

    create(fighter) {
        const {source, _id} = fighter;
        this.id = _id;
        const imageElement = this.createImage(source);

        this.view = this.createElement({tagName: 'div', className: 'fighter'});
        this.view.append(imageElement);
    }

    /**
     * @param {String} source - url for a picture
     * @return {HTMLElement}
     * */
    createImage(source) {
        const attributes = {src: source};
        return this.createElement({
            tagName: 'img',
            className: 'fighter-image',
            attributes
        });
    }

    addData({health, name, attack, defense}) {
        this._name = name;
        this._health = health;
        this._attack = attack;
        this._defense = defense;

        this.hasDescription = true;
    }

    getHitPower() {
        return this._attack * Fighter.getRandInt(1, 2)
    }

    getBlockPower() {
        return this._defense * Fighter.getRandInt(1, 2);
    }

    damage(damage) {
        damage = damage - this.getBlockPower();
        // if my block power > damage , it means that a fighter dodges the blow
        this._health -= (damage > 0) ? damage : 0;
        if (this._health <= 0) this.emit('defeated', this.id);
        else this.emit('updateHealth', this.id);
    }

    static getRandInt(min, max) {
        return Math.random() * (max - min) + min;
    }

    /** @return {Number} */
    get id() {
        return this._id;
    }

    /**@param {Number} id - id of a fighter  */
    set id(id) {
        this._id = id;
    }

    /** @return {HTMLElement} */
    get view() {
        return this._view;
    }

    /**@param {HTMLElement} element */
    set view(element) {
        this._view = element;
    }

    /** @return {Boolean} */
    get hasDescription() {
        return this._hasDescription
    }

    /**@param {Boolean} value */
    set hasDescription(value) {
        this._hasDescription = value;
    }

    /** @return {Boolean} */
    get wasChosen() {
        return this._wasChosen
    }

    /**@param {Boolean} value */
    set wasChosen(value) {
        this._wasChosen = value;
    }

    get name() {
        return this._name;
    }
    get health() {
        return this._health;
    }
}
