import View from "../libs/View";
import Button from "./Button";

export default class Arena extends View {
    constructor() {
        super();

        this._view = null;
        this._fighters = [];
        this._counter = 0;
        this._isActivated = false;
        this._isGameOver = false;
        this.create();
    }

    create() {
        this.view = this.createElement({
            tagName: 'div',
            className: 'arena'
        });

        const button = new Button(null, {
            tagName: 'input',
            className: 'arena-button',
            attributes: {
                'type': 'button',
                'value': 'Fight  !!!!'
            }
        });
        button.view.addEventListener('click', (event) => this.fight());
        this.view.append(button.view);


    }

    addFighter(fighter) {
        this._counter += 1;
        this._fighters.push(fighter);

        fighter.on('defeated', this.gameOver, this);
        fighter.on('updateHealth', this.onUpdateHealth, this);

        if (this._counter === 2) {
            this.activete();
            this.showFighters();
            this.emit('arenaIsActivated');
        }
    }

    showFighters() {
        this._fighters.forEach(fighter => fighter.view.id = 'fighter_on_arena');
        this.view.append(...this._fighters.map(fighter => fighter.view))
    }

    fight() {
        setTimeout(() => {
            this.fightStep();
            if (this._isGameOver) return;
            this.fight();
        }, 500);
    }

    fightStep() {
        const [first, second] = this._fighters;
        first.damage(second.getHitPower());
        second.damage(first.getHitPower());
    }

    gameOver(id) {
        this._isGameOver = true;
        const [fighter] = this._fighters.filter(fighter => fighter.id !== id);
        this.emit('showMessage', fighter.name);
    }

    onUpdateHealth(id) {
        const [fighter] = this._fighters.filter(fighter => fighter.id === id);
        const health = fighter.view.querySelector('.fighter-description > .fighter-health');
        health.children[1].textContent =  fighter.health | 0;
    }

    activete() {
        this._isActivated = true;
        this.view.id = 'activated'
    }

    diactivate(view) {
        this._isActivated = false;
        this.view.id = ''
    }

    get view() {
        return this._view;
    }

    set view(view) {
        this._view = view;
    }


}