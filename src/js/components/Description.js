import View from "../libs/View";

/**
 * @class Description
 * @extends View
 *
 * */

export default class Description extends View {
    /**
     * @param {Object} options - options for description
     * */
    constructor({name, health, attack, defense}) {
        super();
        this._view = null;

        this._isHidden = false;

        this._name = null;
        this._health = null;
        this._attack = null;
        this._defense = null;

        this.create({name, health, attack, defense});

    }

    create(data) {
        this.view = this.createElement({tagName: 'div', className: 'fighter-description'});

        Object.keys(data).forEach((key) => {
            const title = this.getElementWithText({tagName: 'span'}, `${key.toUpperCase()}:`);
            const value = this.getElementWithText({tagName: 'span'}, data[key]);

            this[key] = this.createElement({tagName: 'div', className: `fighter-${key}`});
            this[key].append(title, value);
            this.view.append(this[key]);
        });

    }

    show() {
        this._isHidden = false;
        this._changeVisibility();
    }

    hide() {
        if (this._isHidden) return;
        this._isHidden = true;
        this._changeVisibility();
    }

    _changeVisibility() {
        this.view.classList.toggle('fighter-description-hidden');
    }

    get view() {
        return this._view;
    }

    set view(view) {
        this._view = view;
    }

    get name() {
        return this._name;
    }

    set name(val) {
        this._name = val;
    }

    get health() {
        return this._health;
    }

    set health(val) {
        this._health = val;
    }

    get attack() {
        return this._attack;
    }

    set attack(val) {
        this._attack = val;
    }

    get defense() {
        return this._defense;
    }

    set defense(val) {
        this._defense = val;
    }
}