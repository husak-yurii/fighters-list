import View from "../libs/View";
import Fighter from "./Fighter";
import Description from './Description';
import Button from "./Button";


/**
 * @class FightersContainer
 * @extends View
 *
 * */

export default class FightersContainer extends View {

    constructor() {
        super();
        this._conteiner = null;
        this._fighters = new Map();
        this._descriptions = new Map();
        this._buttons = new Map();

        this.conteiner = this.createElement({tagName: 'div', className: 'fighters-container'});
    }

    /**
     * @param {Array} allFighters - array of fighters' props
     * */
    createAllFighters(allFighters) {
        const views = [];
        const buttonConfig = {
            tagName: 'input',
            className: 'fighter-button',
            attributes: {
                'type': 'button',
                'value': 'Choose',
                'id': 'fighter-button-hidden'
            }
        };

        allFighters.forEach((config) => {
            const fighter = new Fighter(config);
            const button = new Button(fighter.id, buttonConfig);

            fighter.view.append(button.view);
            this._buttons.set(fighter.id, button);
            button.on('fighterChosen', this.onFighterChosen, this);

            views.push(fighter.view);
            this._fighters.set(fighter.id, fighter);
            fighter.on('fighterClicked', this.onFighterClicked, this);

        });

        this.conteiner.append(...views);
    }

    onFighterClicked({event, id}) {
        this.hideAllDescriptions();
        const fighter = this._fighters.get(id);

        if (fighter.wasChosen) return;

        if (!fighter.hasDescription) {
            this.emit('requireFighterData', {id});
            return;
        }

        this._descriptions.get(id).show();
        this._buttons.get(id).show();
    }

    hideAllDescriptions() {
        this._descriptions.forEach(descr => descr.hide());
        this._buttons.forEach(button => button.hide());
    }

    setFightersData(id, data) {
        const {name, health, attack, defense} = data;

        const fighter = this._fighters.get(id);
        fighter.addData({name, health, attack, defense});

        this._descriptions.set(id, new Description({name, health, attack, defense}));
        fighter.view.append(this._descriptions.get(id).view);
        this._buttons.get(id).show();
    }

    onFighterChosen({event, id}) {
        const fighter = this._fighters.get(id);
        if (fighter.wasChosen) return;

        fighter.wasChosen = true;
        this._descriptions.get(id).show();
        this.emit('fighterWasChosen', fighter);
    }

    /** @return {HTMLElement} */
    get conteiner() {
        return this._conteiner;
    }

    /**@param {HTMLElement} element */
    set conteiner(element) {
        this._conteiner = element;
    }


    hide() {
        this.conteiner.id = 'fighters-container-hidden'
    }

}