import Network from "../libs/Network";

/**
 * @class FightersService
 * @extends Network
 *
 * */

export default class FightersService extends Network {
    constructor() {
        super();
        this.baseUrl = '/resources/api/';
    }
    /**
     * To get data about all fighters*/
    getAllFighters() {
        return this.getData('fighters.json', {});
    }

    /**
     * To get more details for chosen fighter*/
    getDetails(id){
        const url = `details/fighter/${id}.json`;
        return this.getData(url, {});
    }
}