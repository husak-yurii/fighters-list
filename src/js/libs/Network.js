/**
 * @class Network
 *
 * */

export default class Network {
    constructor() {
        this._baseUrl = null;
    }
    /**
     * @return {String} a base url address
     * */
    get baseUrl() {
        return this._baseUrl;
    }

    /**
     * @param {String} url - a base url address
     * */
    set baseUrl(url) {
        this._baseUrl = url;
    }
    /**
     * @param {String} [url=""] - a query string
     * @param {Object} [options={}] - options for fetch API
     * @return {Promise}
     * */
    getData(url= '', options = {}) {
        const reqUrl = this._baseUrl.concat(Network.fixUrl(url));
        return fetch(reqUrl, {...options})
            .then(responce => responce.ok ? responce.json() : Promise.reject(Error('Failed to load')))
            .catch(err => console.warn(err));
    }

    static fixUrl(str){
        return this.fixSlashes(this.trim(str));
    }

    static fixSlashes(str){
        return str.replace(/\\/g, "/");
    }

    static trim(str){
      return str.replace(/\s/g, "");
    }
}
