import EventEmitter from 'eventemitter3';

/**
 * @class View
 * @extends EventEmitter
 *
 * */

export default class View extends EventEmitter {
    /**
     * Function expects a config file
     * @param {String} tagName - name of html element which is need to be created
     * @param {String} [className=''] - class name for html element which is being created
     * @param {Object} [attributes={}] - a list of additional attributes for html element which is being created
     *
     * @return {HTMLElement}
     * */
    createElement({tagName, className = '', attributes = {}} = {}) {
        const element = document.createElement(tagName);
        className && element.classList.add(className);
        Object.keys(attributes)
              .forEach(key => element.setAttribute(key, attributes[key]));

        return element;
    }
    /**
     * @param {String} [innerText=''] - inner text of element
     * @param {Object} [options={}]
     *
     * @return {HTMLElement}
     * */
    getElementWithText(options={}, innerText = ''){
        const el = this.createElement(options);
        el.append(document.createTextNode(innerText));
        return el;
    }
}